package main

import (
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"golang.org/x/image/colornames"
	"marc/common"
	"marc/common/messages"
	"time"
)

func run() {
	endPoint, err := common.Connect("localhost:8080")
	if err != nil {
		common.LogError("error connecting to server: %v\n", err)
		return
	}

	cfg := pixelgl.WindowConfig{
		Title:  "Platformer",
		Bounds: pixel.R(0, 0, 400, 400),
		VSync:  true, // causes some mouse lag
	}
	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		common.LogError("error creating pixel window: %v", err)
		return
	}

	imd := imdraw.New(nil)
	imd.Precision = 32

	lastTime := time.Now()
    sendInputTimeAccum := 0.0
    sendCount := 0
	playerPos := pixel.V(0, 0)
	running := true
	for running {
	    dt := time.Since(lastTime).Seconds()
		lastTime = time.Now()
        sendInputTimeAccum += dt

        if sendInputTimeAccum >= 1.0/common.TICK_RATE {
            sendInputTimeAccum = 0.0

            left := false
            right := false
            up := false
            down := false
            if win.Pressed(pixelgl.KeyLeft) || win.Pressed(pixelgl.KeyA) {
                left = true
            }
            if win.Pressed(pixelgl.KeyRight) || win.Pressed(pixelgl.KeyD) {
                right = true
            }
            if win.Pressed(pixelgl.KeyUp) || win.Pressed(pixelgl.KeyW) {
                up = true
            }
            if win.Pressed(pixelgl.KeyDown) || win.Pressed(pixelgl.KeyS) {
                down = true
            }

            playerMsg := messages.PlayerInputMessage{left, right, up, down}
            common.LogInfo("writing msg: %v", playerMsg)
            msg := common.Message{messages.INPUT_MSG_TYPE, playerMsg.Encode()}
            err = messages.WriteMsg(&endPoint, msg)
            sendCount += 1
            common.LogInfo("msg count: %v", sendCount)
            if err != nil {
                common.LogError("error writing msg: %v\n", err)
                return
            }
        }

		var rcvmsg *common.Message
		rcvmsg, err = messages.ReadMsgNoWait(&endPoint)
		if err != nil {
			common.LogError("error reading msg: %v\n", err)
		}
		if rcvmsg != nil {
			var stateMsg messages.GameStateMessage
			err = stateMsg.Decode(rcvmsg.Data)
			if err != nil {
				common.LogError("error decoding game state msg: %v\n", err)
			}
			common.LogInfo("received msg: %v\n", stateMsg)

			playerPos = stateMsg.PlayerList[0]
		}

		win.Clear(colornames.White)
		imd.Clear()
		imd.Color = pixel.RGB(0.9, 0.2, 0.2)
		imd.Push(playerPos)
		imd.Circle(25, 0)
		imd.Draw(win)
		win.Update()

		if win.Closed() || win.JustPressed(pixelgl.KeyEscape) {
			running = false
		}
	}
}

func main() {
	pixelgl.Run(run)
}
