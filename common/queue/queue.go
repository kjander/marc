package queue

import (
	"container/list"
	"sync"
)

type Queue struct {
	list *list.List
	mux  sync.Mutex
}

func New() Queue {
	return Queue{list: list.New()}
}

func (queue *Queue) Put(item interface{}) {
	queue.mux.Lock()
	queue.list.PushBack(item)
	queue.mux.Unlock()
}

func (queue *Queue) Pop() interface{} {
	queue.mux.Lock()
	element := queue.list.Front()
	if element == nil {
		return nil
	}
	value := queue.list.Remove(element)
	queue.mux.Unlock()
	return value
}
