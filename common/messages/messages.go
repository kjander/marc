package messages

import (
    "bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/faiface/pixel"
	"marc/common"
)

const (
	LEFT_BIT_MASK  = 0x01
	RIGHT_BIT_MASK = 0x02
	UP_BIT_MASK    = 0x04
	DOWN_BIT_MASK  = 0x08
)

const (
	INPUT_MSG_TYPE = 1
	STATE_MSG_TYPE = 2
)

type PlayerInputMessage struct {
	Left  bool
	Right bool
	Up    bool
	Down  bool
}

type GameStateMessage struct {
	PlayerList []pixel.Vec
}

func (msg *GameStateMessage) Encode() ([]byte, error) {
    var buf bytes.Buffer

    numPlayersByte := byte(len(msg.PlayerList))
    err := buf.WriteByte(numPlayersByte)
    if err != nil {
        common.LogError("Error writing number of players: %v", err)
        return nil, err
    }

    for _, playerPos := range msg.PlayerList {
        err := binary.Write(&buf, binary.BigEndian, playerPos.X)
        if err != nil {
            common.LogError("Error writing player X: %v", err)
            return nil, err
        }

        err = binary.Write(&buf, binary.BigEndian, playerPos.Y)
        if err != nil {
            common.LogError("Error writing player Y: %v", err)
            return nil, err
        }
    }

    return buf.Bytes(), nil
}

func (msg *GameStateMessage) Decode(data []byte) error {
    buf := bytes.NewBuffer(data)

    numPlayers, err := buf.ReadByte()
    if err != nil {
        common.LogError("Error reading num players: %v", err)
        return err
    }

    msg.PlayerList = make([]pixel.Vec, numPlayers)
    for index, playerPos := range msg.PlayerList {
        err = binary.Read(buf, binary.BigEndian, &playerPos.X)
        if err != nil {
            common.LogError("Error reading player X: %v", err)
            return err
        }

        err = binary.Read(buf, binary.BigEndian, &playerPos.Y)
        if err != nil {
            common.LogError("Error reading player Y: %v", err)
            return err
        }
        msg.PlayerList[index] = playerPos
    }
    return nil
}

func (msg *PlayerInputMessage) Encode() []byte {
	var inputByte byte

	if msg.Left {
		inputByte |= LEFT_BIT_MASK
	}
	if msg.Right {
		inputByte |= RIGHT_BIT_MASK
	}
	if msg.Up {
		inputByte |= UP_BIT_MASK
	}
	if msg.Down {
		inputByte |= DOWN_BIT_MASK
	}

	common.LogInfo("Created input byte: %v", inputByte)

	bytes := make([]byte, 1, 1)
	bytes[0] = inputByte
	return bytes
}

func (msg *PlayerInputMessage) Decode(data []byte) error {
	if len(data) != 1 {
		return errors.New("Player message data wrong size")
	}
	inputByte := data[0]
	common.LogInfo("input byte: %v", inputByte)
	if (LEFT_BIT_MASK & inputByte) != 0 {
		msg.Left = true
	}
	if (RIGHT_BIT_MASK & inputByte) != 0 {
		msg.Right = true
	}
	if (UP_BIT_MASK & inputByte) != 0 {
		msg.Up = true
	}
	if (DOWN_BIT_MASK & inputByte) != 0 {
		msg.Down = true
	}
	return nil
}

func ReadMsgNoWait(endPoint *common.EndPoint) (*common.Message, error) {
	bytes := endPoint.ReceiveNoWait()

	if bytes == nil {
		return nil, nil
	}

	if len(bytes) < 3 {
		return nil, fmt.Errorf("packet size too small: %v", len(bytes))
	}

	msgType := bytes[0]

	common.LogInfo("read message type: %v", msgType)

	dataLengthBytes := bytes[1:3]
	dataLength := binary.BigEndian.Uint16(dataLengthBytes)
	common.LogInfo("read data length: %v", dataLength)

	dataBytes := bytes[3:]
	if uint16(len(dataBytes)) != dataLength {
		return nil, fmt.Errorf("packet size mismatch: %v != %v",
			len(dataBytes), dataLength)
	}

	common.LogInfo("read data")

	msg := common.Message{}
	msg.Type = msgType
	msg.Data = dataBytes
	return &msg, nil
}

func WriteMsg(endPoint *common.EndPoint, msg common.Message) error {
	bytes := make([]byte, 3+len(msg.Data))
	bytes[0] = msg.Type

	dataLength := (uint16)(len(msg.Data))
	binary.BigEndian.PutUint16(bytes[1:3], dataLength)

	copy(bytes[3:], msg.Data)

	err := endPoint.Send(bytes)
	return err
}
