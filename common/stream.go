package common

import (
	"fmt"
	"net"
	"runtime"
    "sync"
)

const (
	TICK_RATE = 3.0
)

const (
	READ_BUFFER_SIZE_PACKETS = 5
	NEW_CONN_CHAN_SIZE       = 10
)

func LogInfo(fmtString string, args ...interface{}) {
	fmt.Printf(fmtString, args...)
	fmt.Printf("\n")
}

func LogWarning(fmtString string, args ...interface{}) {
	fmt.Printf("\033[0;33m")
	fmt.Printf(fmtString, args...)
	fmt.Printf("\n")
	fmt.Printf("\033[0m")
}

func LogError(fmtString string, args ...interface{}) {
	fmt.Printf("\033[0;31m")
	pc, fn, line, _ := runtime.Caller(1)
	fmt.Printf("%s:%d(%s): ", fn, line, runtime.FuncForPC(pc).Name())
	fmt.Printf(fmtString, args...)
	fmt.Printf("\n")
	fmt.Printf("\033[0m")
}

type Message struct {
	Type byte
	Data []byte
}

type Server struct {
	clients      []EndPoint
	udpConn      *net.UDPConn
	newConnChan  chan EndPoint
	freeConnChan chan *EndPoint
    mutex sync.Mutex
    err error
}

type EndPoint struct {
	udpConn    *net.UDPConn
	RemoteAddr *net.UDPAddr
	packetChan chan []byte
	isServer   bool
    doFree bool
}

// Compare IP addr and Port
func equalUDPAddr(addr1, addr2 *net.UDPAddr) bool {
    return addr1.IP.Equal(addr2.IP) && addr1.Port == addr2.Port
}

func (endPoint *EndPoint) Send(dataIn []byte) error {
	var err error
	if endPoint.isServer {
		_, err = endPoint.udpConn.WriteToUDP(dataIn, endPoint.RemoteAddr)
	} else {
		LogInfo("sending %v bytes", len(dataIn))
		var n int
		n, err = endPoint.udpConn.Write(dataIn)
		LogInfo("sent %v bytes", n)
	}
	return err
}

func (endPoint *EndPoint) ReceiveNoWait() []byte {
	select {
	case buf := <-endPoint.packetChan:
		return buf
	default:
		return nil
	}
}

func (server *Server) AcceptConnectNoWait() (*EndPoint, error) {
    server.mutex.Lock()
    if server.err != nil {
        return nil, server.err
    }
    server.mutex.Unlock()

	select {
	case endPoint := <-server.newConnChan:
		return &endPoint, nil
	default:
		return nil, nil
	}
}

// Tell server to stop accepting packets from this endpoint
func (server *Server) FreeConnection(endPoint *EndPoint) {
	server.freeConnChan <- endPoint
}

type serverPacket struct {
    remoteAddr *net.UDPAddr
    buf []byte
}

// goroutine to read all UDP packets into channel
func (server *Server) readPackets(packetChan chan serverPacket, errChan chan error) {
    for {
        // TODO don't allocate for every packet!
		buf := make([]byte, 100)
        n, remoteAddr, err := server.udpConn.ReadFromUDP(buf)
        LogInfo("We got a UDP packet from syscall")
        if err != nil {
            LogError("Error reading UDP packet: %v", err)
            errChan <- err
            return
        }
        buf = buf[:n]
        packetChan <- serverPacket{remoteAddr, buf}
    }
}

func (server *Server) Run() {
    // start receving each packet into a channel
    packetChan := make(chan serverPacket)
    errChan := make(chan error)
    go server.readPackets(packetChan, errChan)

	for {
        // wait for something to do
        select {
            case err := <- errChan:
                server.mutex.Lock()
                server.err = fmt.Errorf("stream server error: %v", err)
                LogError("%v", server.err)
                server.mutex.Unlock()
                return
            case finishedEndPoint := <- server.freeConnChan:
                LogInfo("server got a free conn")
                // Remove the endpoint
                lastIndex := len(server.clients) - 1
                for i, endPoint := range server.clients {
                    if equalUDPAddr(endPoint.RemoteAddr, finishedEndPoint.RemoteAddr) {
                        server.clients[i], server.clients[lastIndex] = server.clients[lastIndex], server.clients[i]
                        break
                    }
                }
                server.clients = server.clients[:lastIndex]
            case packet := <-packetChan:
                LogInfo("Received a UDP packet")
                data := packet.buf
                remoteAddr := packet.remoteAddr

                foundIndex := -1
                for index, client := range server.clients {
                    if equalUDPAddr(client.RemoteAddr, remoteAddr) {
                        foundIndex = index
                    }
                }

                var endPoint EndPoint
                if foundIndex != -1 {
                    endPoint = server.clients[foundIndex]
                } else {
                    LogInfo("Packet received from new addr: %v", remoteAddr)
                    endPoint = EndPoint{
                        server.udpConn,
                        remoteAddr,
                        make(chan []byte, READ_BUFFER_SIZE_PACKETS),
                        true,
                        false,
                    }
                    server.clients = append(server.clients, endPoint)
                    server.newConnChan <- endPoint
                }
                select {
                case endPoint.packetChan <- data:
                default:
                    // drop packets if it exceeds channel capacity
                    LogWarning("channel capacity exceeded, dropping packet")
                }
        }


	}
    LogInfo("Server stopped")
}

func StartServer(address string) (Server, error) {
	localAddr, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		return Server{}, err
	}
	udpConn, err := net.ListenUDP("udp", localAddr)
	clients := make([]EndPoint, 0, 10)
	newConnChan := make(chan EndPoint, NEW_CONN_CHAN_SIZE)
	server := Server{
		clients,
		udpConn,
		newConnChan,
		make(chan *EndPoint),
        sync.Mutex{},
        nil,
	}
	go server.Run()
	return server, err
}

func clientReceive(endPoint EndPoint) {
	for {
		buf := make([]byte, 100)
		n, _, err := endPoint.udpConn.ReadFromUDP(buf)
		if err != nil {
			LogError("Error reading UDP packet: %v", err)
		}
		data := buf[0:n]
		select {
		case endPoint.packetChan <- data:
		default:
			LogWarning("channel capacity exceeded, dropping packet")
		}
	}
}

func Connect(address string) (EndPoint, error) {
	remoteAddr, err := net.ResolveUDPAddr("udp", address)
	if err != nil {
		return EndPoint{}, err
	}
	conn, err := net.DialUDP("udp", nil, remoteAddr)
	endPoint := EndPoint{
		conn,
		remoteAddr,
		make(chan []byte, READ_BUFFER_SIZE_PACKETS),
		false,
        false,
	}
	go clientReceive(endPoint)
	return endPoint, err
}
