package main

import (
	"github.com/faiface/pixel"
	"marc/common"
	"marc/common/messages"
	"time"
)

/*
TODO
	- don't reallocate bytes for message write
    - reuse UDPPacket objects instead of constant GC
    - UDP, not tcp
	- handle errors in read/write client messages
		- propogate error messages?
    - drop packets instead of blocking on adding them to an endpoints channel
*/

const (
	CLIENT_TIMEOUT_SECS = 6
	MAX_NUM_PLAYERS     = 16
)

type Player struct {
	Pos              pixel.Vec
	Vel              pixel.Vec
	EndPoint         *common.EndPoint
	LastMsgTimeStamp time.Time
}

var playerList = make([]Player, 0, MAX_NUM_PLAYERS)

func main() {
	server, err := common.StartServer("localhost:8080")
	if err != nil {
		common.LogError("Error starting server: %v", err)
		return
	}

	readCount := 0
	lastTime := time.Now()
	for {
		dt := time.Since(lastTime).Seconds()
		lastTime = time.Now()
		common.LogInfo("Waiting for new player")
		endPoint, err := server.AcceptConnectNoWait()
        if err != nil {
            common.LogError("Server had error")
            return
        }
		if endPoint != nil {
			common.LogInfo("New player joined from: %v", endPoint.RemoteAddr)
			newPlayer := Player{
				pixel.V(100, 100),
				pixel.V(0, 0),
				endPoint,
				time.Now()}
			playerList = append(playerList, newPlayer)
		}

		timedOutPlayerIndexList := make([]int, 0, MAX_NUM_PLAYERS)
		for index, player := range playerList {
			messageWasReceived := false
			for {
				msg, err := messages.ReadMsgNoWait(player.EndPoint)
				if err != nil {
					common.LogError("Error reading msg: %v", err)
					break
				}
				if msg == nil {
					break
				}
				messageWasReceived = true
				readCount += 1
				common.LogInfo("msg count: %v", readCount)
				var playerMsg messages.PlayerInputMessage
				err = playerMsg.Decode(msg.Data)
				if err != nil {
					common.LogError("Error decoding msg: %v", err)
				}
				common.LogInfo("player msg: %v", playerMsg)

				player.Vel = pixel.ZV
				if playerMsg.Left && !playerMsg.Right {
					player.Vel.X = -1
				} else if playerMsg.Right && !playerMsg.Left {
					player.Vel.X = 1
				}
				if playerMsg.Up && !playerMsg.Down {
					player.Vel.Y = 1
				} else if playerMsg.Down && !playerMsg.Up {
					player.Vel.Y = -1
				}
			}

			if messageWasReceived {
				player.LastMsgTimeStamp = time.Now()
			} else {
				if time.Since(player.LastMsgTimeStamp).Seconds() > CLIENT_TIMEOUT_SECS {
					common.LogInfo("client timed out")
					timedOutPlayerIndexList = append(timedOutPlayerIndexList, index)
					continue
				}
			}

			player.Pos = player.Pos.Add(player.Vel.Scaled(150 * dt))
			playerList[index] = player
		}
        common.LogInfo("Finished player msg loop")

		// remove timed out players, release connection
		for _, index := range timedOutPlayerIndexList {
			server.FreeConnection(playerList[index].EndPoint)
			playerList[index], playerList[len(playerList)-1] = playerList[len(playerList)-1], playerList[index]
		}
		playerList = playerList[:len(playerList)-len(timedOutPlayerIndexList)]
        common.LogInfo("num players: %v", len(playerList))

		if len(playerList) > 0 {
			playerPosList := make([]pixel.Vec, len(playerList), len(playerList))
			for i := 0; i < len(playerList); i++ {
				playerPosList[i] = playerList[i].Pos
			}
			stateMsg := messages.GameStateMessage{playerPosList}
			encodedMsg, err := stateMsg.Encode()
			if err != nil {
				common.LogError("error encoding state message: %v", err)
			}
			common.LogInfo("Writing msg: %v", stateMsg)
			msg := common.Message{messages.STATE_MSG_TYPE, encodedMsg}

			for _, player := range playerList {
				err = messages.WriteMsg(player.EndPoint, msg)
				if err != nil {
					common.LogError("error writing game state msg: %v\n", err)
					return
				}
			}
		}

		// TODO (lose resolution when multiply by 1000000, specify nanoseconds instead)
		// is there not a better way to create duration?
		var frameTime float64 = 1.0 / common.TICK_RATE
		remainingFrameTime := time.Duration(frameTime*float64(time.Second)) - time.Since(lastTime)
		if remainingFrameTime < 0 {
			common.LogWarning("server cannot keep up with tick rate")
		}
		time.Sleep(remainingFrameTime)
	}
}
